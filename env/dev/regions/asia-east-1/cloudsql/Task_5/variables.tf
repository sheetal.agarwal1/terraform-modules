variable "project_id" {
  description = "The ID of the project in which resources will be provisioned."
  type        = string
}

#variable "network" {
# type        = string
# description = "A reference to the network to which this router belongs"
#}

variable "region" {
  type        = string
  description = "The GCP region in which this router belongs to"
}

variable "db_name" {
  description = "The name of the SQL Database instance"
  default     = "priya-mysql-public"
}

variable "authorized_networks" {
  default = [{
    name  = "priya-gcp-health-checkers-range"
    value = "130.211.0.0/28"
  }]
  type        = list(map(string))
  description = "List of mapped public networks authorized to access to the instances. Default - short range of GCP health-checkers IPs"
}
