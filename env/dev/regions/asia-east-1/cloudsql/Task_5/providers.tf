provider "google" {
  project = var.project_id
  region  = var.region
  #network = var.vpc_network
}
