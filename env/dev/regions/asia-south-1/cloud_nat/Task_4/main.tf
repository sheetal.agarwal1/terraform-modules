module "cloud-nat" {
  source     = "../../../../../../modules/networking/terraform-google-cloud-nat"
  router     = var.router
  project_id = var.project_id
  region     = var.region
  name       = "priya-cloud-nat"
}
