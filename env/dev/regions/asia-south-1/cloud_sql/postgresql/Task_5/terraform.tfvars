project_id              = "searce-playground-v1"
vpc_network             = "projects/searce-playground-v1/global/networks/saurabh-team-vpc"
region                  = "asia-south1"
#zone                    = "asia-south1-a"
instance_name           = "priya-tf-postgresql1"
instance_type           = "db-custom-1-3840"
instance_primary_zone   = "asia-south1-a"
availability_type       = "REGIONAL"
#maintenance_window_hour = 23
#maintenance_window_day  = 1

database_flags = [
  {
    name  = "autovacuum"
    value = "off"
  },
]

user_labels = {
  owner = "priya-soni"
}


root_user = "root"
root_password = "password"

additional_users = []

// Configuration for read replicas
read_replicas = [
  {
    name                = "priya-tf-postgresql-readreplica"
    zone                = "asia-south1-b"
    tier                = "db-custom-1-3840"̃
    disk_autoresize     = false
    disk_size           = 10
    disk_type           = "PD_HDD"
    user_labels         = { env = "dev", type = "replica"}
    encryption_key_name = null
    retained_backups    = 0
    database_flags      = []
    ip_configuration = {
      ipv4_enabled        = true
      require_ssl         = false
      private_network     = "projects/searce-playground-v1/global/networks/saurabh-team-vpc"
      authorized_networks = []
    }
  }
]

insights_config = {
  query_string_length     = 1024
  record_application_tags = true
  record_client_address   = true
}
