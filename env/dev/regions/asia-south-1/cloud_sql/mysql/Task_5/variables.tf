variable "project_id" {
  type        = string
  description = "The project to setup cloudsql mysql on"
}

variable "vpc_network" {
  description = "Name of the VPC network to peer."
  type        = string
}

variable "region" {
  type        = string
  description = "The region to setup cloudsql mysql in"
}


variable "location" {
  type        = string
  description = "The region to setup cloudsql mysql in"
  default     = "asia-south1-a"
}


variable "instance_name" {
  type        = string
  description = "The name of the cloudsql mysql instance"
}

variable "instance_type" {
  type        = string
  description = "The type of the cloudsql mysql instance"
}

variable "instance_primary_zone" {
  type        = string
  description = "The primary zone of the cloudsql mysql instance. (a,b,c)"
}

variable "availability_type" {
  type        = string
  description = "The availability_type of the cloudsql mysql instance. REGIONAL or ZONAL"
}

variable "disk_size" {
  description = "The disk size for the master instance"
  type        = number
  default     = 10
}

variable "disk_type" {
  description = "The disk type for the master instance."
  type        = string
  default     = "PD_SSD"
}

variable "database_flags" {
  type        = list
  description = "The database_flags of the cloudsql mysql instance."
}

variable "user_labels" {
  type        = map
  description = "The user_labels of the cloudsql mysql instance."
}

variable "root_user" {
  type        = string
  description = "The root user of the cloudsql mysql instance."
  default     = "root"
}

variable "root_password" {
  type        = string
  description = "The root password of the cloudsql mysql instance."
  default     = "password"
}

variable "additional_users" {
  description = "The users apart from root of the cloudsql mysql instance."
}

variable "maintenance_window_hour" {
  description = "The hour of day (0-23) maintenance window for the master instance maintenance."
  type        = number
  default     = 23 
}

variable "maintenance_window_day" {
  description = "The day of week (1-7) for the master instance maintenance."
  type        = number
  default     = 1
}

variable "backup_configuration" {
  description = "The backup_configuration settings subblock for the database setings"
  type = object({
    binary_log_enabled             = bool
    enabled                        = bool
    start_time                     = string
    location                       = string
    transaction_log_retention_days = string
    retained_backups               = number
    retention_unit                 = string
  })
  default = {
    binary_log_enabled             = false
    enabled                        = false
    start_time                     = null
    location                       = null
    transaction_log_retention_days = null
    retained_backups               = null
    retention_unit                 = null
  }
}

variable "read_replicas" {
  description = "List of read replicas to create. Encryption key is required for replica in different region. For replica in same region as master set encryption_key_name = null"
  type = list(object({
    name            = string
    tier            = string
    zone            = string
    disk_type       = string
    disk_autoresize = bool
    disk_size       = string
    user_labels     = map(string)
    database_flags = list(object({
      name  = string
      value = string
    }))
    ip_configuration = object({
      authorized_networks = list(map(string))
      ipv4_enabled        = bool
      private_network     = string
      require_ssl         = bool
    })
    encryption_key_name = string
  }))
  default = []
}

variable "read_replica_configuration" {
  description = "The replica configuration for use in all read replica instances."
  type = object({
    connect_retry_interval    = number
    dump_file_path            = string
    ca_certificate            = string
    client_certificate        = string
    client_key                = string
    failover_target           = bool
    master_heartbeat_period   = number
    password                  = string
    ssl_cipher                = string
    username                  = string
    verify_server_certificate = bool
  })
  default = {
    connect_retry_interval    = null
    dump_file_path            = null
    ca_certificate            = null
    client_certificate        = null
    client_key                = null
    failover_target           = null
    master_heartbeat_period   = null
    password                  = null
    ssl_cipher                = null
    username                  = null
    verify_server_certificate = null
  }
}

variable "read_replica_name_suffix" {
  description = "The optional suffix to add to the read instance name"
  type        = string
  default     = ""
}

variable "read_replica_size" {
  description = "The size of read replicas"
  type        = number
  default     = 0
}

variable "read_replica_tier" {
  description = "The tier for the read replica instances."
  type        = string
  default     = ""
}

variable "read_replica_zones" {
  description = "The zones for the read replica instancess, it should be something like: `a,b,c`. Given zones are used rotationally for creating read replicas."
  type        = string
  default     = ""
}

variable "read_replica_activation_policy" {
  description = "The activation policy for the read replica instances. Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  type        = string
  default     = "ALWAYS"
}

variable "read_replica_crash_safe_replication" {
  description = "The crash safe replication is to indicates when crash-safe replication flags are enabled."
  type        = bool
  default     = true
}

variable "read_replica_disk_autoresize" {
  description = "Configuration to increase storage size."
  type        = bool
  default     = true
}

variable "read_replica_disk_size" {
  description = "The disk size for the read replica instances."
  type        = number
  default     = 10
}

variable "read_replica_disk_type" {
  description = "The disk type for the read replica instances."
  type        = string
  default     = "PD_SSD"
}

variable "read_replica_pricing_plan" {
  description = "The pricing plan for the read replica instances."
  type        = string
  default     = "PER_USE"
}

variable "read_replica_replication_type" {
  description = "The replication type for read replica instances. Can be one of ASYNCHRONOUS or SYNCHRONOUS."
  type        = string
  default     = "SYNCHRONOUS"
}

variable "read_replica_database_flags" {
  description = "The database flags for the read replica instances. See [more details](https://cloud.google.com/sql/docs/mysql/flags)"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "read_replica_maintenance_window_day" {
  description = "The day of week (1-7) for the read replica instances maintenance."
  type        = number
  default     = 1
}

variable "read_replica_maintenance_window_hour" {
  description = "The hour of day (0-23) maintenance window for the read replica instances maintenance."
  type        = number
  default     = 23
}

variable "read_replica_maintenance_window_update_track" {
  description = "The update track of maintenance window for the read replica instances maintenance. Can be either `canary` or `stable`."
  type        = string
  default     = "canary"
}

variable "read_replica_user_labels" {
  type        = map(string)
  default     = {}
  description = "The key/value labels for the read replica instances."
}

variable "read_replica_ip_configuration" {
  description = "The ip configuration for the read replica instances."
  type = object({
    authorized_networks = list(map(string))
    ipv4_enabled        = bool
    private_network     = string
    require_ssl         = bool
  })
  default = {
    authorized_networks = []
    ipv4_enabled        = true
    private_network     = null
    require_ssl         = null
  }
}

