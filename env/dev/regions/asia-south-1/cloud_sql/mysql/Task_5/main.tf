module "mysql" {
  source           = "../../../../../../../modules/cloudsql/mysql"
  name             = var.instance_name
  project_id       = var.project_id
  database_version = "MYSQL_5_7"
  region           = var.region

  // Master configurations
  tier                            = var.instance_type
  zone                            = var.instance_primary_zone
  availability_type               = var.availability_type
  disk_size                       = var.disk_size
  disk_type                       = var.disk_type
  maintenance_window_day          = var.maintenance_window_day 
  maintenance_window_hour         = var.maintenance_window_hour
  maintenance_window_update_track = "stable"
  #database_flags                  = var.database_flags
  user_labels                     = var.user_labels
  read_replicas                   = var.read_replicas

  ip_configuration      = {
    ipv4_enabled        = true
    require_ssl         = false
    private_network     = "projects/searce-playground-v1/global/networks/saurabh-team-vpc"
    authorized_networks = []
  }

  backup_configuration = {
    enabled            = true
    binary_log_enabled = true
    start_time         = "00:00"
    location           = var.location
    transaction_log_retention_days = 5
    retained_backups               = 2
    retention_unit                 = "COUNT"
  }

  user_name     = var.root_user
  user_password = var.root_password

  additional_users = var.additional_users
}



