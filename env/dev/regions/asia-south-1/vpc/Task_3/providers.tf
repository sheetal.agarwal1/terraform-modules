provider "google" {
  project = var.project_id
  region  = "asia-east1"
}
