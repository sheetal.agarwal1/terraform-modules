
/* source       = "env/dev/regions/asia-south-1/vpc/Task_3/modules/networking/terraform-google-network/" */

module "priya-vpc" {
  source = "../../../../../../modules/networking/vpc_working/terraform-google-network/"
  #version = “~> 2.0.0”
  project_id   = var.project_id
  network_name = var.network_name
  mtu          = 1460
  subnets          = var.subnets
  secondary_ranges = var.secondary_ip_ranges
}