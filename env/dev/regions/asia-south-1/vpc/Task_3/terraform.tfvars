project_id = "searce-playground-v1"
network_name = "priya-vpc1"
subnets = [
  {
    subnet_name   = "subnet-01"
    subnet_ip     = "10.10.10.0/24"
    subnet_region = "asia-east1"
    subnet_private_access = "true"
    subnet_flow_logs = "false"
  },
  {  
    subnet_name   = "subnet-02"
    subnet_ip     = "10.10.20.0/24"
    subnet_region = "asia-east1"
    subnet_private_access = "true"
    subnet_flow_logs = "false"

  },
  {
    subnet_name   = "subnet-03"
    subnet_ip     = "10.10.30.0/24"
    subnet_region = "asia-east1"
#    subnet_private_access = "true"
    subnet_flow_logs = "false"
    purpose = "INTERNAL_HTTPS_LOAD_BALANCER"
    role    = "ACTIVE"
  },
]
secondary_ip_ranges = {
    subnet-01 = [
      {
        range_name    = "subnet-01-01"
        ip_cidr_range = "192.168.1.0/24"
      },
      {
        range_name    = "subnet-01-02"
        ip_cidr_range = "192.168.2.0/24"
      },
    ]
    subnet-02 = []
  }
