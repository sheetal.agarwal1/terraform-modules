
variable "project_id" {
  description = "The project that the service account will be created in."
}

variable "roles" {
  description = "The camel case role id to use for this role."
}

variable "role_name" {
  description = "The camel case role id to use for this role."
}

variable "permissions" {
  description = "The names of the permissions this role grants when bound in an IAM policy. "
}
