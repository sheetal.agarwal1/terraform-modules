project_id = "searce-playground-v1"
roles = [
{
  role_name   = "prod_infra_mgmt_gce_role"
  permissions = [
    "roles/compute.admin",
    "roles/compute.networkAdmin",
    "roles/compute.storageAdmin",
    "roles/container.admin",
    "roles/iam.roleAdmin",
    "roles/iam.serviceAccountAdmin",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/iam.serviceAccountUser",
    "roles/resourcemanager.projectIamAdmin",
    "roles/storage.admin"
    ]
},

{
  role_name   = "prod_gke_cluster_role"
  permissions = [
    "logging.logEntries.create",
    "monitoring.metricDescriptors.create",
    "monitoring.metricDescriptors.get",
    "monitoring.metricDescriptors.list",
    "monitoring.monitoredResourceDescriptors.get",
    "monitoring.monitoredResourceDescriptors.list",
    "monitoring.timeSeries.create",
    "monitoring.timeSeries.list",
    "resourcemanager.projects.get",
    "storage.objects.get",
    "storage.objects.list"
   ]
},

{
  role_name   = "prod_gke_apps_role"
  permissions = [
   "storage.buckets.get",
    "storage.buckets.list",
    "storage.buckets.getIamPolicy",
    "storage.buckets.setIamPolicy",
    "storage.buckets.update",
    "storage.objects.create",
    "storage.objects.get",
    "storage.objects.list",
    "resourcemanager.projects.get",
    "secretmanager.versions.access"
   ]  
},

{
  role_name   = ""
  permissions = [
   "roles/iam.serviceAccountUser",
    "roles/logging.logWriter",
    "roles/storage.admin",
    "roles/run.admin",
    "roles/secretmanager.secretAccessor",
    "roles/artifactregistry.writer"
   ]
}
]