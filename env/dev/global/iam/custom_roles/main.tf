module "my_iam_custom_role" {
  source   = "../../../../../modules/iam/custom_role"
  count   = length(var.roles)
  project     = var.project_id
  role_id     = var.roles[count.index]["role_name"]
  #stage       = "GA"
  permissions = var.roles[count.index]["permissions"]
}



