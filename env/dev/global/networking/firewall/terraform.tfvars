/******************************************
	Default values for terraform
 *****************************************/

project_id   = "searce-playground-v1"
network_name = "dev-main-vpc"
rules = [{
    name                    = "dev-main-vpc-allow-iap-access-fw"
    description             = "Generic Firewall rules for Cloud IAP Access"
    direction               = "INGRESS"
    priority                = 1000
    ranges                  = ["35.235.240.0/20"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = ["allow-iap-access"]
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = ["22"]
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  },
  {
    name                    = "dev-main-vpc-allow-lb-hc-fw"
    description             = "GCP LB Healthchecks"
    direction               = "INGRESS"
    priority                = 1000
    ranges                  = ["209.85.204.0/22","209.85.152.0/22","130.211.0.0/22","35.191.0.0/16"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = null
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = []
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  },
  {
    name                    = "dev-main-vpc-default-deny-fw"
    description             = "default deny rule"
    direction               = "INGRESS"
    priority                = 65535
    ranges                  = ["0.0.0.0/0"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = null
    target_service_accounts = null
    allow = []
    deny = [{
      protocol = "tcp"
      ports    = []
    }]
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }]
